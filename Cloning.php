<?php

  class Posts {
    public $post;

    function __construct($post) {
      $this->post = $post;
    }
  }

  $post1 = new Posts("First post");
  //Copy by reference (just copy pointer, not data)
  $post2 = $post1;
  $post1->post = "Modified post.";
  echo $post2->post . "<br>";

  $post2->post = "Also modified post.";
  echo $post1->post . "<br>";
